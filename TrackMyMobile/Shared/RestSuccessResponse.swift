//
//  RestSuccessResponse.swift
//  TrackMyMobile
//
//  Created by Seun O on 6/6/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit

public struct RestSuccessResponse: Codable {
    let message: String
}
