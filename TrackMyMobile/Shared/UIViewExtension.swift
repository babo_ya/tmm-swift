//
//  UIViewExtension.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 5/15/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit

extension UIView {
    func layoutUIViewToParentView(parentView: UIView,
                                  padding: UIEdgeInsets = UIEdgeInsets(top: 10, left: 15, bottom: 0, right: 15),
                                  height: CGFloat = 55.0
                                  ) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.topAnchor,
                                  constant: padding.top).isActive = true
        self.leadingAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.leadingAnchor,
                                      constant: padding.left).isActive = true
        self.widthAnchor.constraint(equalTo: parentView.widthAnchor,
                                    constant: -30)
        self.rightAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.rightAnchor,
                                    constant: -padding.right).isActive = true
        self.heightAnchor.constraint(equalToConstant: height).isActive = true
    }
    
    func layoutUIViewBelowView(belowView: UIView,
                               padding: UIEdgeInsets = UIEdgeInsets(top: 10, left: 15, bottom: 0, right: 15),
                               height: CGFloat = 55.0
        ) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: belowView.safeAreaLayoutGuide.bottomAnchor,
                                  constant: padding.top).isActive = true
        self.leadingAnchor.constraint(equalTo: belowView.safeAreaLayoutGuide.leadingAnchor).isActive = true
        self.rightAnchor.constraint(equalTo: belowView.safeAreaLayoutGuide.rightAnchor).isActive = true
        self.heightAnchor.constraint(equalToConstant: height).isActive = true
    }
}
