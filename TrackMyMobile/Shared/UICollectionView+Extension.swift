//
//  UICollectionView+Extension.swift
//  NWYC
//
//  Created by Seyun O on 11/21/18.
//  Copyright © 2018 Seyun O. All rights reserved.
//

import UIKit

extension UICollectionView {
    func register<T: UICollectionViewCell>(_: T.Type) where T: ReuseableCell {
        self.register(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T where T: ReuseableCell {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
        }
        return cell
    }
    
    func registerReuseableView<T: UICollectionReusableView>(_: T.Type, kind: String) where T: ReuseableCell {
        self.register(T.self, forSupplementaryViewOfKind: kind, withReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func dequeueReusableView<T: UICollectionReusableView>(forIndexPath indexPath: IndexPath, kind: String) -> T where T: ReuseableCell {
        guard let reusableView = dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue reusableView")
        }
        return reusableView
    }
    
    func updateCellAtIndexPath(_ indexPath: IndexPath, completion: @escaping () -> Void) {
        self.performBatchUpdates({
            self.deleteItems(at: [indexPath])
            self.insertItems(at: [indexPath])
        }) { (success) in
            completion()
        }
    }
    
    func updateFirstSection() {
        self.performBatchUpdates({
            let section = IndexSet(integer: 0)
            self.deleteSections(section)
            self.insertSections(section)
        }) { (success) in
        }
    }
    
    func updateCollectionSectionAt(_ index: Int) {
        self.performBatchUpdates({
            let section = IndexSet(integer: index)
            self.deleteSections(section)
            self.insertSections(section)
        }) { (success) in
        }
    }
    func updateSection(_ index: Int) {
        self.performBatchUpdates({
            let section = IndexSet(integer: index)
            self.deleteSections(section)
            self.insertSections(section)
        }) { (success) in
        }
    }
}

protocol ReuseableCell: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReuseableCell where Self: UIView {
    static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

class AppCollectionCell: UICollectionViewCell, ReuseableCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureCell() {
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}


class AppCollectionReuseableView: UICollectionReusableView, ReuseableCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureView() {
        
    }
}
