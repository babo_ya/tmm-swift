//
//  Util.swift
//  NWYC
//
//  Created by Seyun O on 2/23/19.
//  Copyright © 2019 Seyun O. All rights reserved.
//

import UIKit

func getDeviceInformation() -> String?
{
    guard let info = Bundle.main.infoDictionary else { return nil }
    guard let version = info["CFBundleShortVersionString"] as? String else { return nil }
    guard let bundle = info["CFBundleVersion"] as? String else { return nil }
    let deivceName = UIDevice.current.model
    let os = UIDevice.current.systemVersion
    return "version. \(version) #\(bundle) \(deivceName) \(os)"
}

func getWindowWidth() -> CGFloat {
    let screen = UIScreen.main.bounds
    return screen.width
}

func getWindowHeight() -> CGFloat {
    let screen = UIScreen.main.bounds
    return screen.height
}

func getCellWidth() -> CGFloat {
    let screen = UIScreen.main.bounds
    return screen.width
}

func getCellHeight() -> CGFloat {
    let screen = UIScreen.main.bounds
    return screen.height
}

func getSizeWithHeight(height: CGFloat) -> CGSize {
    let screen = UIScreen.main.bounds
    return CGSize(width: screen.width, height: height)
}
