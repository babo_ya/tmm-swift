//
//  AppTextField.swift
//  CV
//
//  Created by Seyun O on 6/30/17.
//  Copyright © 2017 App. All rights reserved.
//

import UIKit

enum TextFieldValidataion {
    case valid
    case required
    case inValid(errorMessage: String)
    case warning(warningMessage: String)
    
    func getLabelColor() -> UIColor {
        switch self {
        case .valid:
            return AppColor.primaryColor.getColor
        case .required:
            return AppColor.errorColor.getColor
        case .inValid(_):
            return AppColor.errorColor.getColor
        case .warning(_):
            return AppColor.errorColor.getColor
        }
    }
}

class AppTextField: UIView {
    
    var bottomLayer: CALayer!
    var textField: UITextField!
    var textFieldHintLabel: UILabel!
    var labelTopConstraint: NSLayoutConstraint!
    var textFieldAttributeString: NSAttributedString? {
        didSet {
            guard let t = self.textFieldAttributeString else { return }
            textFieldHintLabel.attributedText = t
        }
    }
    
    var validation: TextFieldValidataion? {
        didSet {
            guard let textFieldValidation = self.validation else { return }
            textFieldHintLabel.textColor = textFieldValidation.getLabelColor()
            textFieldHintLabel.numberOfLines = 2
            bottomLayer.backgroundColor = textFieldValidation.getLabelColor().cgColor
            
            switch textFieldValidation {
            case .valid:
                break
            case .required:
                break
            case let .inValid(errorMessage):
                print("error..\(errorMessage)")
            case .warning(_):
                break
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    convenience init () {
        self.init(frame:CGRect.zero)
    }
    
    init(frame: CGRect, delegate: UITextFieldDelegate) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureView() {
        textFieldHintLabel = UILabel()
        textFieldHintLabel.font = AppFont.tinyBoldFont.getFont
        textFieldHintLabel.textColor = AppColor.mediumLabelColor.getColor
        textFieldHintLabel.isUserInteractionEnabled = false
        textFieldHintLabel?.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(textFieldHintLabel)
        textFieldHintLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        textFieldHintLabel.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -20).isActive = true
        labelTopConstraint = textFieldHintLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: -15)
        labelTopConstraint.isActive = true
        
        bottomLayer = CALayer()
        bottomLayer.backgroundColor = AppColor.dividerColor.getColor.cgColor
        self.layer.addSublayer(bottomLayer)
        
        textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = AppFont.textFieldFont.getFont
        textField.textColor = AppColor.darkLabelColor.getColor
        self.addSubview(textField)
        textField.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        textField.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -30).isActive = true
        textField.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        textField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.returnKeyType = .done
        textField.delegate = self
    }
    
    func showPlaceHolderLabel(_ show: Bool) {
        if show == true {
            labelTopConstraint.constant = -15
            bottomLayer.backgroundColor = AppColor.primaryColor.getColor.cgColor
            textFieldHintLabel.textColor = AppColor.primaryColor.getColor
            textFieldHintLabel.font = AppFont.tinyBoldFont.getFont
            UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.6,
                                       initialSpringVelocity: 0.8, options: .curveEaseIn, animations: {
                                        self.layoutIfNeeded()
            }) { (success) in
                
            }
            
            guard let textFieldValidation = self.validation else { return }
            bottomLayer.backgroundColor = textFieldValidation.getLabelColor().cgColor
            textFieldHintLabel.textColor = textFieldValidation.getLabelColor()
            
        } else {
            textFieldHintLabel.textColor = AppColor.mediumLabelColor.getColor
            bottomLayer.backgroundColor = AppColor.dividerColor.getColor.cgColor
            if self.textField.text?.count == 0 {
                labelTopConstraint.constant = 0
                UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.6,
                                           initialSpringVelocity: 0.8, options: .curveEaseIn, animations: {
                                            self.layoutIfNeeded()
                }) { (success) in
                    
                }
            } else {
                
            }
        }
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        bottomLayer.frame = CGRect(x: 0, y: self.frame.height - 15, width: self.frame.width, height: 1)
    }
}

extension AppTextField: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.showPlaceHolderLabel(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.showPlaceHolderLabel(false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
