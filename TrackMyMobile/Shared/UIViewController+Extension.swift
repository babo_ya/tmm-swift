//
//  UIViewController+Extension.swift
//  NWYC
//
//  Created by Seyun O on 11/23/18.
//  Copyright © 2018 Seyun O. All rights reserved.
//

import UIKit

protocol ViewServiceRequestResponder {
    func handleShowError(error: HTTPRemoteError)
    func handleServiceRequestCompleted()
    func showServiceRequestLoading()
}

enum LoadingType {
    case loading
    case overlay
    case bottomLoading
}

/*
extension UIViewController: ViewServiceRequestResponder {
    
    func handleServiceRequestCompleted() {
        
    }
    
    func handleShowError(error: HTTPRemoteError) {
        
    }
    
    func showServiceRequestLoading() {
        
        
    }
}
 */
