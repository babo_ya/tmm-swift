//
//  ApplicationActiveStateObserving.swift
//  MyViveraeiOS
//
//  Created by Seyun oh on 6/17/16.
//  Copyright © 2016 Viverae, inc. All rights reserved.
//

import UIKit


protocol ObserverTokenStore : class {
    func addObserverToken(token: NSObjectProtocol)
}

protocol ApplicationActiveStateObserving : ObserverTokenStore {
    func performGroupedBlock(block: () -> ())
    func applicationDidBecomeActive()
    func applicationDidEnterBackground()
}

extension ApplicationActiveStateObserving {
    func setupApplicationActiveNotifications() {
        
        addObserverToken(token: NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification,
                                                                       object: nil, queue: nil) { [weak self] note in
            guard let observer = self else { return }
            observer.performGroupedBlock {
                observer.applicationDidEnterBackground()
            }
            })
        
        addObserverToken(token: NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
                                                                       object: nil, queue: nil) { [weak self] note in
            guard let observer = self else { return }
            observer.performGroupedBlock {
                observer.applicationDidBecomeActive()
            }
            })
        if UIApplication.shared.applicationState == .active {
            applicationDidBecomeActive()
        }
    }
}
