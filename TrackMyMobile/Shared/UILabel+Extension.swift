//
//  UILabel+Extension.swift
//  NWYC
//
//  Created by Seyun O on 11/20/18.
//  Copyright © 2018 Seyun O. All rights reserved.
//

import UIKit

let kLABEL_LINE_HEIGHT: CGFloat = 1.15
let kTITLE_LABEL_LINE_HEIGHT: CGFloat = 1.1

extension UILabel {
    static func bodyLabelCreator(parentView: UIView) -> UILabel {
        let label: UILabel = UILabel()
        parentView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = AppColor.darkLabelColor.getColor
        label.font = AppFont.bodyFont.getFont
        label.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: 15).isActive = true
        label.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 10).isActive = true
        label.widthAnchor.constraint(equalTo: parentView.widthAnchor, constant: 30).isActive = true
        return label
    }
    
    static func labelCreator(parentView: UIView, color: UIColor, font: UIFont) -> UILabel {
        let label: UILabel = UILabel()
        parentView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = color
        label.font = font
        label.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: 15).isActive = true
        label.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 10).isActive = true
        label.widthAnchor.constraint(equalTo: parentView.widthAnchor, constant: 30).isActive = true
        return label
    }
    
    func setLineHeight(lineHeight: CGFloat) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.0
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.alignment = self.textAlignment
        
        let attrString = NSMutableAttributedString()
        if (self.attributedText != nil) {
            attrString.append( self.attributedText!)
        } else {
            attrString.append( NSMutableAttributedString(string: self.text!))
            if let f = self.font {
                attrString.addAttribute(NSAttributedString.Key.font, value: f, range: NSMakeRange(0, attrString.length))
            }
        }
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }
}

extension UITextView {
    func setLineHeight(lineHeight: CGFloat) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.0
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.alignment = self.textAlignment
        
        let attrString = NSMutableAttributedString()
        if (self.attributedText != nil) {
            attrString.append( self.attributedText!)
        } else {
            attrString.append( NSMutableAttributedString(string: self.text!))
            if let f = self.font {
                attrString.addAttribute(NSAttributedString.Key.font, value: f, range: NSMakeRange(0, attrString.length))
            }
        }
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }
}

class PaddedTextField: UITextField {
    
    var inset: CGFloat = 15
    var insetY: CGFloat = 10
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: inset, dy: insetY)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}
