//
//  Button+Extension.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 6/7/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit

extension UIButton {
    func applyTheme() {
        self.layer.borderWidth = 0.5
        self.layer.borderColor = AppColor.primaryColor.getColor.cgColor
        self.setTitleColor(AppColor.primaryColor.getColor, for: .normal)
        self.titleLabel?.font = AppFont.smallBoldFont.getFont
        
    }
    
    func errorButton() {
        self.backgroundColor = AppColor.primaryColor.getColor
        self.setTitleColor(.white, for: .normal)
    }
    
    func applyDarkTheme() {
        self.backgroundColor = AppColor.primaryColor.getColor
        self.setTitleColor(.white, for: .normal)
        self.titleLabel?.font = AppFont.bodyBold.getFont
    }
    
    func cleanButton() {
        self.titleLabel?.font = AppFont.smallBoldFont.getFont
        self.setTitleColor(AppColor.primaryColor.getColor, for: .normal)
    }
    
    func disableButton() {
        self.backgroundColor = AppColor.mediumLabelColor.getColor
        self.setTitleColor(.white, for: .normal)
    }
}
