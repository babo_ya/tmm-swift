//
//  String+Extension.swift
//  NWYC
//
//  Created by Seyun O on 11/23/18.
//  Copyright © 2018 Seyun O. All rights reserved.
//

import UIKit

extension String {
    public func getHTML() -> NSAttributedString? {
        var encodedData: Data!
        encodedData = self.data(using: String.Encoding.utf32)!
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
            NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue
        ]
        do {
            let attribute = try NSAttributedString(data: encodedData, options: options, documentAttributes: nil)
            return attribute
        } catch {
            return nil
        }
    }
    
    public func trimHTMLTags() -> String? {
        guard let data = self.data(using: .utf8) else {
            return self
        }
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return self
        }
        
        return attributedString.string
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        return ceil(boundingBox.width)
    }
    
    /*
    public func getBioAttributedString() -> NSAttributedString? {
        var encodedData: Data!
        encodedData = self.data(using: String.Encoding.utf32)!
        let attributedOptions = [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType] as [String : Any]
        
        do {
            let attribute = try NSMutableAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
            attribute.addAttributes([NSFontAttributeName : AppFont.smallFont.getFont,
                                     NSForegroundColorAttributeName : AppColor.darkLabelColor.getColor], range: NSMakeRange(0, attribute.length))
            return attribute
        } catch {
            return nil
        }
    }
    
    public func sizeWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGSize {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                            attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.size
    }
    
    func substring(location: Int, length: Int) -> String? {
        guard characters.count >= location + length else { return nil }
        let start = index(startIndex, offsetBy: location)
        let end = index(startIndex, offsetBy: location + length)
        return substring(with: start..<end)
    }
 */
}
