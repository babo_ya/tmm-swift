//
//  NSManagedObject+Extension.swift
//  MyViveraeiOS
//
//  Created by Seyun oh on 6/17/16.
//  Copyright © 2016 Viverae, inc. All rights reserved.
//

import CoreData
extension NSManagedObject {
    public func refresh(_ mergeChanges: Bool = true) {
        managedObjectContext?.refresh(self, mergeChanges: mergeChanges)
    }
    
}
