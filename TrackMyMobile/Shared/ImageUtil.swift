//
//  ImageUtil.swift
//  NWYC
//
//  Created by Seyun O on 2/11/19.
//  Copyright © 2019 Seyun O. All rights reserved.
//

import UIKit

func yom_saveToCacheWithImageUrl(imageData: NSData, imageUrl: String) {
    if !imageUrl.isEmpty {
        let splittedStrings: [String] = imageUrl.components(separatedBy: "/")
        
        if splittedStrings.count > 0 {
            if let fileName = splittedStrings.last {
                let cacheFolderPath:String = NSSearchPathForDirectoriesInDomains(.cachesDirectory,
                                                                                    .userDomainMask, true)[0]
                let pathToImage = cacheFolderPath + "/" + fileName
                imageData.write(toFile: pathToImage, atomically:true)
            }
        }
    }
}

func yom_imageFromCacheWithImageUrl(imageUrl: String) -> UIImage? {
    var resultImage: UIImage?
    
    if !imageUrl.isEmpty {
        let splittedStrings: [String] = imageUrl.components(separatedBy: "/")
        
        if splittedStrings.count > 0 {
            if let fileName = splittedStrings.last {
                let cacheFolderPath:String = NSSearchPathForDirectoriesInDomains(.cachesDirectory,
                                                                                    .userDomainMask, true)[0]
                let pathToImage = cacheFolderPath + "/" + fileName
                let url: NSURL = NSURL(fileURLWithPath: pathToImage)
                    
                do {
                    let imageData = try NSData(contentsOf: url as URL, options:
                            NSData.ReadingOptions.uncached)
                    resultImage = UIImage(data: imageData as Data)!
                } catch _ as NSError {
                }
            }
        }
    }
    return resultImage;
}

public extension UIImage {
    func resizeImageWithNewwidth(_ newWidth: CGFloat) -> UIImage {
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}


func yom_dataFromCacheWithImageUrl(imageUrl: String) throws-> NSData {
    var error: NSError! = NSError(domain: "Migrator", code: 0, userInfo: nil)
    
    if !imageUrl.isEmpty {
        let splittedStrings: [String] = imageUrl.components(separatedBy: "/")
        
        if splittedStrings.count > 0 {
            if let fileName = splittedStrings.last {
                let cacheFolderPath:String = NSSearchPathForDirectoriesInDomains(.cachesDirectory,
                                                                                    .userDomainMask, true)[0]
                let pathToImage = cacheFolderPath + "/" + fileName
                let url: NSURL = NSURL(fileURLWithPath: pathToImage)
                    
                do {
                    let imageData = try NSData(contentsOf: url as URL, options:
                            NSData.ReadingOptions.uncached)
                    return imageData
                } catch let error1 as NSError {
                        error = error1
                }
            }
        }
    }
    
    throw error
}
