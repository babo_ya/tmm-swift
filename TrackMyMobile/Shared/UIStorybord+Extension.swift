//
//  UIStorybord+Extension.swift
//  NWYC
//
//  Created by Seyun O on 11/20/18.
//  Copyright © 2018 Seyun O. All rights reserved.
//

import UIKit

extension UIStoryboard {
    static func generateViewControllerWithName(storyboardName: String) -> UIViewController {
        let storyboard: UIStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        let nav = UINavigationController(rootViewController: vc!)
        return nav
    }
}
