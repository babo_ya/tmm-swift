//
//  Notifications.swift
//  NWYC
//
//  Created by Seyun O on 2/23/19.
//  Copyright © 2019 Seyun O. All rights reserved.
//

import UIKit

protocol NotificationName {
    var name: Notification.Name { get }
}

extension RawRepresentable where RawValue == String, Self: NotificationName {
    var name: Notification.Name {
        get {
            return Notification.Name(self.rawValue)
        }
    }
}

enum Notifications: String, NotificationName {
    case showLogin
    case accessDenied
    case logOut
    case loggedIn
}
