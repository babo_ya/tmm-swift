//
//  Date+Extension.swift
//  NWYC
//
//  Created by Seyun O on 11/24/18.
//  Copyright © 2018 Seyun O. All rights reserved.
//

import UIKit
extension Date {
    var year: Int {
        return Calendar.current.component(.year, from: self)
    }
}



extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
