//
//  RestErrorResponse.swift
//  TrackMyMobile
//
//  Created by Seun O on 6/6/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit

public struct RestErrorResponse: Codable {
    let message: String
}


extension Encodable {
    public var toDictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}

extension Array where Element: Encodable {
    
    public var toArrayOfDictionaries: [[String: Any]] {
        return compactMap({ $0.toDictionary ?? nil }) as [[String: Any]]
    }
}
