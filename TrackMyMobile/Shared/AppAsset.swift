//
//  AppAsset.swift
//  OrthoPhoto
//
//  Created by Seyun oh on 4/13/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit

enum AppColor {
    case primaryColor
    case secondaryColor
    case thirdColor
    case darkLabelColor
    case errorColor
    case dividerColor
    case mediumLabelColor
    case greenColor
    case redColor
    case veryLightColor
    case lightColor
    case borderColor
    case orangeColor
    case yellowColor
}

extension AppColor {
    var getColor: UIColor {
        switch self {
        case .primaryColor:
            return UIColor(red:0.00, green:0.36, blue:0.72, alpha:1.0)
        case .secondaryColor:
            return UIColor(red:0.63, green:0.19, blue:0.13, alpha:1.0)
        case .thirdColor:
            return UIColor(red:0.91, green:0.34, blue:0.34, alpha:1.0)
        case .darkLabelColor:
            return UIColor(red:0.27, green:0.27, blue:0.27, alpha:1.0)
        case .mediumLabelColor:
            return UIColor(red:0.5, green:0.5, blue:0.5, alpha:1.0)
        case .errorColor:
            return UIColor(red:0.84, green:0.00, blue:0.21, alpha:1.0)
        case .dividerColor:
            return UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
        case .greenColor:
            return UIColor(red:0.53, green:0.79, blue:0.34, alpha:1.0)
        case .redColor:
            return UIColor(red:0.91, green:0.34, blue:0.34, alpha:1.0)
        case .veryLightColor:
            return UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.0)
        case .borderColor:
            return UIColor(red:0.97, green:0.97, blue:0.97, alpha:1.0)
        case .lightColor:
            return UIColor(red:0.67, green:0.67, blue:0.67, alpha:1.0)
        case .orangeColor:
            return UIColor(red:0.98, green:0.55, blue:0.16, alpha:1.0)
        case .yellowColor:
            return UIColor(red:0.95, green:0.75, blue:0.03, alpha:1.0)
        }
    }
}

enum AppFont {
    case bodyFont
    case bodyBold
    case bodyThin
    case headerBold
    case buttonFont
    case navigationBarFont
    case largeNavigationFont
    case textFieldFont
    case tinyBoldFont
    case smallBoldFont
    case smallThinFont
    case smallFont
    case verySmallBoldFont
    case verySmallThinFont
    case veryLargeThinFont
    case rtaFont
    case superLargeThinFont
    case menuFont
    case detailFont
}

extension AppFont {
    var getFont: UIFont {
        switch self {
        case .headerBold:
            return UIFont(name: "HelveticaNeue-Bold", size: 22)!
        case .bodyBold:
            return UIFont(name: "HelveticaNeue-Bold", size: 18)!
        case .bodyFont:
            return UIFont(name: "HelveticaNeue", size: 18)!
        case .bodyThin:
            return UIFont(name: "HelveticaNeue-Light", size: 18)!
        case .buttonFont:
            return UIFont(name: "HelveticaNeue-Bold", size: 16)!
        case .navigationBarFont:
            return UIFont(name: "HelveticaNeue-Bold", size: 16)!
        case .largeNavigationFont:
            return UIFont(name: "HelveticaNeue-Bold", size: 26)!
        case .textFieldFont:
            return UIFont(name: "HelveticaNeue-Bold", size: 16)!
        case .tinyBoldFont:
            return UIFont(name: "HelveticaNeue", size: 12)!
        case .smallBoldFont:
            return UIFont(name: "HelveticaNeue-Bold", size: 15)!
        case .verySmallBoldFont:
            return UIFont(name: "HelveticaNeue-Bold", size: 13)!
        case .verySmallThinFont:
            return UIFont(name: "HelveticaNeue-Light", size: 13)!
        case .smallFont:
            return UIFont(name: "HelveticaNeue", size: 15)!
        case .detailFont:
            return UIFont(name: "HelveticaNeue", size: 16)!
        case .smallThinFont:
            return UIFont(name: "HelveticaNeue-Light", size: 14)!
        case .veryLargeThinFont:
            return UIFont(name: "Helvetica-Light", size: 22)!
        case .rtaFont:
            return UIFont(name: "AvenirNextCondensed-Bold", size: 32)!
        case .superLargeThinFont:
            return UIFont(name: "HelveticaNeue-Thin", size: 34)!
        case .menuFont:
            return UIFont(name: "HelveticaNeue-Bold", size: 17)!
        }
    }
}

func applyApplicationTheme() {
    UINavigationBar.appearance().tintColor = AppColor.primaryColor.getColor
 
    UITabBar.appearance().barTintColor = UIColor.white
    UINavigationBar.appearance().barTintColor = UIColor.white
    UITabBar.appearance().barTintColor = UIColor.white
    
    UIBarButtonItem.appearance().setTitleTextAttributes(
        [
            NSAttributedString.Key.font : AppFont.bodyBold.getFont,
            NSAttributedString.Key.foregroundColor : AppColor.primaryColor.getColor,
        ], for: .normal)
}
