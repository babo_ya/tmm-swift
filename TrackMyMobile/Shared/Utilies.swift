//
//  Utilies.swift
//  CV
//
//  Created by Seyun oh on 5/12/17.
//  Copyright © 2017 Viverae. All rights reserved.
//

import UIKit
import CoreData

public enum Result<A> {
    case success(A)
    case failure(HTTPRemoteError)
}


extension NSMutableURLRequest {
    static func createURLRequest(_ params: Dictionary<String, String>) {
        
    }
}

func loadJson(_ fileName: String) -> NSDictionary? {
    if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
        if let data = try? Data(contentsOf: url) {
            do {
                let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)  as? NSDictionary
                return object
            } catch {
                print("Error!! Unable to parse  \(fileName).json")
            }
        }
        print("Error!! Unable to load  \(fileName).json")
    }
    return nil
}

extension Data {
    func printData() {
        let dataString = NSString(data: self, encoding: String.Encoding.utf8.rawValue)
        print(dataString ?? "")
    }
}

public enum HTTPRemoteError: Error {
    case unKnownError
    case parseError
    case noDataError
    case accessDenied
    case requireUserAction
    case badStatusCode(statusCode: Int)
    case missingData
    case serverError(errorMessage: String)
    case unableToFindRecord(errorMessage: String)
    case requiresTwoFactorAuthenication
    case apiError(error: RestErrorResponse)
    case exceededAttempts
    case unableToFindData
}

extension HTTPRemoteError: CustomStringConvertible {
    public var description: String {
        switch self {
        case .unKnownError:
            return "An error has occurred. Please check your internet connection and try again."
        case .parseError:
            return "Parse Error"
        case .requiresTwoFactorAuthenication:
            return "Two Factor Authenication Required"
        case .noDataError:
            return "No Data Error"
        case let .badStatusCode(statusCode):
            return "Bad Status Code \(statusCode)"
        case .accessDenied:
            return "Access Denied"
        case .requireUserAction:
            return "Require User Action"
        case .missingData:
            return "Missing Information"
        case let .serverError(errorMessage):
            return errorMessage
        case let .unableToFindRecord(errorMessage):
            return errorMessage
        case .exceededAttempts:
            return "Exceeded Attempts"
        case .unableToFindData:
            return "Unable to find the data"
        case let .apiError(error):
            return error.message
        }
    }
}




