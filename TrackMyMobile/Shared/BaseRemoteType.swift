//
//  BaseRemoteType.swift
//  NWYC
//
//  Created by Seyun O on 11/21/18.
//  Copyright © 2018 Seyun O. All rights reserved.
//

import UIKit

public let ROOT_API_SERVICE_URL: String = "https://5tckl4dwv5.execute-api.us-east-1.amazonaws.com/dev"
public let API_CLIENT_KEY: String = "AKIAXXZND5SU4X6YMJTZ"
public let API_CLIENT_SECRET_KEY: String = "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"


protocol BaseURLRequestable {
    func getBaseURLRequest(urlString: String) -> URLRequest?
}

extension BaseURLRequestable {
    func getBaseURLRequest(urlString: String) -> URLRequest? {
        if let url = URL(string: urlString) {
            let urlRequest = URLRequest(url: url)
            return urlRequest
        }
        return nil
    }
}
