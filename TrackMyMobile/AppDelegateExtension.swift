//
//  AppDelegateExtension.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 5/15/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit
import FirebaseCore
import UserNotifications
import FirebaseMessaging

extension AppDelegate {
    func showLoginViewController() {
        self.window?.makeKeyAndVisible()
        
        let storyboard: UIStoryboard = UIStoryboard(name: "login", bundle: nil)
        let viewController = storyboard.instantiateInitialViewController() as! LoginViewController
        viewController.configure(with: LoginViewModel(managedObjectContext: self.persistentContainer.viewContext,
                                                      syncCoordinator: self.userSyncCoordinator))
        let nav = UINavigationController(rootViewController: viewController)
        nav.navigationBar.isHidden = true
        self.window?.rootViewController?.present(nav,
                                                 animated: true, completion: nil)
    }
    
    func configureCommonData() {
        self.userSyncCoordinator = UserSyncCoordinator(managedObjectContext: self.persistentContainer.viewContext)
        self.deviceSyncCoordinator = DeviceSyncCoordinator(managedObjectContext: self.persistentContainer.viewContext)
        applyApplicationTheme()
    }
    
    func configureFirebase(application: UIApplication) {
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            let openAction = UNNotificationAction(identifier: "OpenNotification", title: "Details", options: UNNotificationActionOptions.foreground)
            let eVoterCategory = UNNotificationCategory(identifier: "NWYCEVoterPush", actions: [openAction], intentIdentifiers: [], options: [])
            let articleCategory = UNNotificationCategory(identifier: "NWYCArticlePush", actions: [openAction], intentIdentifiers: [], options: [])
            let reportCategory = UNNotificationCategory(identifier: "NWYCReportPush", actions: [openAction], intentIdentifiers: [], options: [])
            UNUserNotificationCenter.current().setNotificationCategories(Set([eVoterCategory, articleCategory, reportCategory]))
            
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
    }
}


extension AppDelegate : UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let notification: UNNotification = response.notification
        //self.handleAppLaunchFromNotification(notification.request.content.userInfo as NSDictionary)
   
        /*
        let dict = notification.request.content.userInfo
        print(dict)
        if let viewId = dict["viewId"] as? String {
            if let notificationType = NWYCNotificationType(rawValue: viewId) {
                notificationType.handleShowObject(objectId: dict["objectId"], delegate: self)
            }
        }
        
        if let objectId = dict["objectId"] as? String {
            print("Object ID : \(objectId)")
        }
 */
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler(
            [UNNotificationPresentationOptions.alert,
             UNNotificationPresentationOptions.sound,
             UNNotificationPresentationOptions.badge])
    }
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("..")
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("..")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("..>.")
    }
}

extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
        
        print("Firebase Device Token : \(fcmToken)")
        
        /*
        self.userSyncCoordinator.updatePushToken(pushToken: fcmToken) { (result) in
            switch result {
            case .success(_):
                break
            case .failure(_):
                break
            }
        }
 */
    }
}
