//
//  DeviceSyncCoordinator.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 6/10/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit
import CoreData

public final class DeviceSyncCoordinator: AppSyncCoordinator {
    
    var moc: NSManagedObjectContext
    fileprivate var setupToken = Int()
    var didSetup: Bool { return setupToken != 0 }
    
    private lazy var __once: () = {
        self.performGroupedBlock(block: {
            self.setupApplicationActiveNotifications()
        })
    }()

    func setup() {
        _ = self.__once
        self.setupToken = 1
    }
    
    init(managedObjectContext moc: NSManagedObjectContext) {
        self.moc = moc
    }
}

extension DeviceSyncCoordinator: ApplicationActiveStateObserving {
    func applicationDidBecomeActive() {
        
    }
    
    func applicationDidEnterBackground() {
    }
    
    func performGroupedBlock(block: () -> ()) {
        block()
    }
    
    func addObserverToken(token: NSObjectProtocol) {
        
    }
}

