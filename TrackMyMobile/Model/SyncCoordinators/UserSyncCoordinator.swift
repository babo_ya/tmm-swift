//
//  UserSyncCoordinator.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 5/15/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit
import CoreData

protocol AppSyncCoordinator {
    var moc: NSManagedObjectContext { get set }
}

public final class UserSyncCoordinator: AppSyncCoordinator {
    
    var moc: NSManagedObjectContext
    var registerRemote: RegisterUserRemote
    var loginRemote: LoginUserRemote
    
    fileprivate var setupToken = Int()
    var didSetup: Bool { return setupToken != 0 }
    private lazy var __once: () = {
        self.performGroupedBlock(block: {
            self.setupApplicationActiveNotifications()
        })
    }()
    
    func setup() {
        _ = self.__once
        self.setupToken = 1
    }
    
    init(managedObjectContext moc: NSManagedObjectContext) {
        self.moc = moc
        self.registerRemote = RegisterUserRemote()
        self.loginRemote = LoginUserRemote()
    }
    
    typealias RemoteRegisterUserCompletion = (Result<Bool?>) -> ()
    func registerUser(userName: String, password: String,
                      email: String,
                      completion: @escaping RemoteRegisterUserCompletion) {
        
        registerRemote.fetchRegisterUser(userName: userName, password: password,
                                         email: email) { [weak self] (result) in
            guard let strongSelf = self else { return }
            switch result {
            case let .success(response):
                DispatchQueue.main.async {
                    if let password = response.input.Item.password {
                        strongSelf.saveUserInfo(userName: userName,
                                          password: password,
                                          response: response)
                        completion(Result.success(true))
                        return
                    }
                    completion(Result.success(true))
                }
                break
            case let .failure(e):
                DispatchQueue.main.async {
                    completion(Result.failure(e))
                }
                break
            }
        }
    }
    
    typealias RemoteLoginUserCompletion = (Result<Bool?>) -> ()
    func loginUser(userName: String, p: String, completion: @escaping RemoteLoginUserCompletion) {
        loginRemote.fetchLoginUser(userName: userName, password: p) { (result) in
            switch result {
            case let .success(response):
                DispatchQueue.main.async {
                    if let password = response.input.Item.password {
                        if password == p {
                            self.saveUserInfo(userName: userName,
                                              password: password,
                                              response: response)
                            completion(Result.success(true))
                            return
                        }
                    }
                    completion(Result.failure(HTTPRemoteError.unableToFindRecord(errorMessage: "Unable to authencate. Please check your user name & password")))
                }
                break
            case let .failure(e):
                DispatchQueue.main.async {
                    completion(Result.failure(e))
                }
                break
            }
        }
    }
    
    private func saveUserInfo(userName: String, password: String, response: LoginRemoteResponse) {
        KeychainManager.saveItem(itemType: .password, value: password)
        KeychainManager.saveItem(itemType: .userName, value: userName)
        KeychainManager.saveItem(itemType: .deviceId, value: UIDevice.current.identifierForVendor!.uuidString)
        
        if let userId = response.input.Item.userId {
            KeychainManager.saveItem(itemType: .userId, value: userId)
        }
    }
}

extension UserSyncCoordinator: ApplicationActiveStateObserving {
    func applicationDidBecomeActive() {

    }
    
    func applicationDidEnterBackground() {
    }
    
    func performGroupedBlock(block: () -> ()) {
        block()
    }
    
    func addObserverToken(token: NSObjectProtocol) {
        
    }
}
