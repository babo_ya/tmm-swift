//
//  RegisterUserRemote.swift
//  TrackMyMobile
//
//  Created by Seun O on 6/6/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit

import UIKit

public typealias RegisterUserCompletion = (Result<LoginRemoteResponse>) -> ()

struct RegisterUserRemote: BaseURLRequestable {
    var userName: String!
    var password: String!
    var email: String!
    
    mutating func fetchRegisterUser(userName: String, password: String, email: String,
                                    completion: @escaping RegisterUserCompletion) {
        self.userName = userName
        self.password = password
        self.email = email
        
        guard let request = self.getURLRequest() else { return }
        
        URLSession.shared.dataTask(with: request) { (responseData, response, error) in
            guard let httpResponse = response as? HTTPURLResponse, 200...204 ~= httpResponse.statusCode else {
                if let remoteResponse = response as? HTTPURLResponse {
                    switch remoteResponse.statusCode {
                    case 401:
                        completion(Result.failure(HTTPRemoteError.accessDenied))
                    case 204:
                        completion(Result.failure(HTTPRemoteError.noDataError))
                        break
                    case 422, 400, 404:
                        guard let data = responseData else {
                            completion(Result.failure(HTTPRemoteError.unKnownError))
                            return
                        }
                        do {
                            let errorResponse = try JSONDecoder().decode(RestErrorResponse.self, from: data)
                            completion(Result.failure(HTTPRemoteError.apiError(error: errorResponse)))
                            return
                        } catch {
                            print(error)
                        }
                    case 500:
                        responseData?.printData()
                        completion(Result.failure(HTTPRemoteError.unKnownError))
                        return
                    default:
                        break;
                    }
                } else {
                    completion(Result.failure(HTTPRemoteError.badStatusCode(statusCode: -1)))
                    return
                }
                return
            }
            guard let data = responseData else {
                completion(Result.failure(HTTPRemoteError.unKnownError))
                return
            }
            do {
                let successResponse = try JSONDecoder().decode(LoginRemoteResponse.self, from: data)
                completion(Result.success(successResponse))
                return
            } catch {
                print(error)
            }
            }.resume()
    }
}

extension RegisterUserRemote {
    func getURLRequest() -> URLRequest? {
        
        guard let userName = self.userName else { return nil }
        guard let password = self.password else { return nil }
        guard let email = self.email else { return nil }
        
        let urlString = "https://5tckl4dwv5.execute-api.us-east-1.amazonaws.com/dev/user"
        var urlRequest = self.getBaseURLRequest(urlString: urlString)
        urlRequest?.httpMethod = "POST"
        var bodyData = Dictionary<String, Any>()
        bodyData["userName"] = userName
        bodyData["password"] = password
        bodyData["email"] = email
        
        do {
            let postData: Data = try JSONSerialization.data(withJSONObject: bodyData, options: .prettyPrinted)
            urlRequest?.httpBody = postData
        } catch {
            
        }
        
        guard let signedRequest = URLRequestSigner().signWithPost(request: urlRequest!, secretSigningKey: "LdTEZvyjth6HILeLicmeKiO4ney6EmyE0crxb5LK", accessKeyId: "AKIAXXZND5SU4X6YMJTZ") else {
            return nil
        }
        
        return signedRequest
    }
}
