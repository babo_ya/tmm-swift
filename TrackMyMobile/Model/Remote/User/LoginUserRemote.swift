//
//  LoginUserRemote.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 6/8/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit

public typealias LoginUserCompletion = (Result<LoginRemoteResponse>) -> ()

struct LoginUserRemote: BaseURLRequestable {
    var userName: String!
    var userPassword: String?
    var email: String!
    
    mutating func fetchLoginUser(userName: String, password: String,
                                    completion: @escaping LoginUserCompletion) {
        self.userName = userName
        self.userPassword = password
        
        guard let request = self.getURLRequest() else { return }
        
        URLSession.shared.dataTask(with: request) { (responseData, response, error) in
            guard let httpResponse = response as? HTTPURLResponse, 200...204 ~= httpResponse.statusCode else {
                if let remoteResponse = response as? HTTPURLResponse {
                    switch remoteResponse.statusCode {
                    case 401:
                        completion(Result.failure(HTTPRemoteError.accessDenied))
                    case 204:
                        completion(Result.failure(HTTPRemoteError.noDataError))
                        break
                    case 422, 400, 404:
                        guard let data = responseData else {
                            completion(Result.failure(HTTPRemoteError.unKnownError))
                            return
                        }
                        do {
                            let errorResponse = try JSONDecoder().decode(RestErrorResponse.self, from: data)
                            completion(Result.failure(HTTPRemoteError.apiError(error: errorResponse)))
                            return
                        } catch {
                            print(error)
                        }
                    case 500:
                        responseData?.printData()
                        completion(Result.failure(HTTPRemoteError.unKnownError))
                        return
                    default:
                        break;
                    }
                } else {
                    completion(Result.failure(HTTPRemoteError.badStatusCode(statusCode: -1)))
                    return
                }
                return
            }
            guard let data = responseData else {
                completion(Result.failure(HTTPRemoteError.unKnownError))
                return
            }
            do {
                let successResponse = try JSONDecoder().decode(LoginRemoteResponse.self, from: data)
                completion(Result.success(successResponse))
                return
            } catch {
                print(error)
            }
            }.resume()
    }
}

extension LoginUserRemote {
    func getURLRequest() -> URLRequest? {
        guard let userName = self.userName else { return nil }
        
        let urlString = "\(ROOT_API_SERVICE_URL)/user?userName=\(userName)"
        var urlRequest = self.getBaseURLRequest(urlString: urlString)
        urlRequest?.httpMethod = "GET"
        
        guard let signedRequest = URLRequestSigner().sign(request: urlRequest!, secretSigningKey: API_CLIENT_SECRET_KEY, accessKeyId: API_CLIENT_KEY) else {
            return nil
        }
        
        return signedRequest
    }
}
