//
//  LoginRemoteResponse.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 6/9/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit

public struct LoginRemoteResponse : Codable {
    
    let message: String
    let input: LoginRemoteInputResponse
    
}

public struct LoginRemoteInputResponse: Codable {
    var Item: LoginRemoteInputUserResponse
}

public struct LoginRemoteInputUserResponse: Codable {
    let userName: String?
    let password: String?
    let email: String?
    let userId: String?
}

/*
 {"message":"Success","input":{"Item":{"userName":"young","createdAt":1559747688158,"password":"password","loggedAt":1559747688158,"email":"seyun.o@gmail.com","userId":"a86c7fe0-87a4-11e9-af3a-07aafcbc1610"}}}

 */
