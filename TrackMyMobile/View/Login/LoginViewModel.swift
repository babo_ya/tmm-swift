//
//  LoginViewModel.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 5/13/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CoreData

enum LoginUiState {
    case loading
    case none
    case success
    case validated
    case invalid(error: LoginViewError?)
    case failure(error: String?)
}

enum LoginViewError: Error {
    case missingUserName
    case missingEmail
    case missingPassword
    case missingFirstName
    case missingLastName
    case loginFailed(errorMessage: String)
}

extension LoginViewError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .missingEmail:
            return "Email Address is missing"
        case .missingLastName:
            return "Last Name is missing"
        case .missingPassword:
            return "Password is missing"
        case .missingFirstName:
            return "First Name is missing"
        case .missingUserName:
            return "User name is missing"
        case let .loginFailed(errorMessage):
            return errorMessage
        }
    }
}

struct LoginViewModel {
    let moc: NSManagedObjectContext
    let syncCoordinator: UserSyncCoordinator
    let userNameBehavior = BehaviorRelay<String>(value: "")
    let passwordBehavior = BehaviorRelay<String>(value: "")
    
    let uiStateObservable: PublishSubject<LoginUiState> = PublishSubject<LoginUiState>()
    let disposeBag: DisposeBag = DisposeBag()

    init(managedObjectContext moc: NSManagedObjectContext, syncCoordinator: AppSyncCoordinator) {
        self.moc = moc
        self.syncCoordinator = syncCoordinator as! UserSyncCoordinator
        configureUiObservables()
    }
    
    private func configureUiObservables() {
        userNameBehavior.subscribe(onNext: { userName in
            self.validateUserData()
        }).disposed(by: disposeBag)
        
        passwordBehavior.subscribe(onNext: { password in
            self.validateUserData()
        }).disposed(by: disposeBag)
    }

    private func validateUserData() {
        let userName = userNameBehavior.value
        let password = passwordBehavior.value
        
        guard !userName.isEmpty, userName.count > 2 else {
            uiStateObservable.onNext(LoginUiState.invalid(error: LoginViewError.missingUserName))
            return
        }
        
        guard !password.isEmpty, password.count > 2 else {
            uiStateObservable.onNext(LoginUiState.invalid(error: LoginViewError.missingPassword))
            return
        }
        
        uiStateObservable.onNext(LoginUiState.validated)
    }
    
    //public
    func loginUser() {
        uiStateObservable.onNext(LoginUiState.loading)
        syncCoordinator.loginUser(userName: userNameBehavior.value,
                                  p: passwordBehavior.value) { (result) in
            switch (result) {
            case .success(_):
                self.uiStateObservable.onNext(LoginUiState.success)
                break
            case let .failure(e):
                self.uiStateObservable.onNext(LoginUiState.failure(error: e.description))
                break
            }
        }
    }
}
