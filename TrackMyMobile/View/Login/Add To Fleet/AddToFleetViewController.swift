//
//  AddToFleetViewController.swift
//  TrackMyMobile
//
//  Created by Seun O on 5/21/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AddToFleetViewController: UIViewController {
    
    private(set) var viewModel: AddToFleeViewModel!
    var issuesPinCode: AppTextField!
    var emailTextField: AppTextField!
    var addToFleetButton: UIButton!
    var keyboardHeight: CGFloat?
    var addToFleetConstraint: NSLayoutConstraint!
    var currentTextField: UITextField?
    
    private var disposableBags: DisposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add To Fleet"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        let navigationBar = navigationController!.navigationBar
        navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func configure(with viewModel: AddToFleeViewModel) {
        self.viewModel = viewModel
        updateUi()
        configureViewModel()
    }
    
    func updateUi() {
        issuesPinCode = AppTextField()
        issuesPinCode.textFieldHintLabel.text = "Issued Pin Code"
        self.view.addSubview(issuesPinCode)
        issuesPinCode.textField.isSecureTextEntry = true
        issuesPinCode.textField.autocapitalizationType = .none
        issuesPinCode.translatesAutoresizingMaskIntoConstraints = false
        issuesPinCode.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        issuesPinCode.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 130).isActive = true
        issuesPinCode.heightAnchor.constraint(equalToConstant: 55).isActive = true
        issuesPinCode.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        issuesPinCode.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: -30).isActive = true
        
        emailTextField = AppTextField()
        emailTextField.textFieldHintLabel.text = "Email Address"
        self.view.addSubview(emailTextField)
        emailTextField.layoutUIViewBelowView(belowView: self.issuesPinCode)
        emailTextField.textField.autocapitalizationType = .none

        addToFleetButton = UIButton()
        self.view.addSubview(addToFleetButton)
        addToFleetButton.setTitle("Add me to fleet", for: .normal)
        addToFleetButton.setTitleColor(AppColor.primaryColor.getColor, for: .normal)
        addToFleetButton.disableButton()
        addToFleetButton.translatesAutoresizingMaskIntoConstraints = false
        addToFleetButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
        addToFleetButton.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        addToFleetButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        addToFleetConstraint = addToFleetButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor,
                                                                    constant: 0)
        addToFleetConstraint?.isActive = true
        self.view.layoutIfNeeded()
    }
    
    func configureViewModel() {
        issuesPinCode.textField.rx.text
            .orEmpty
            .bind(to: viewModel.issuedCode)
            .disposed(by: disposableBags)
        
        emailTextField.textField.rx.text
            .orEmpty
            .bind(to: viewModel.emailBehavior)
            .disposed(by: disposableBags)
        
        addToFleetButton.rx.tap
            .bind {
                self.viewModel.sendAddToFleet()
            }
            .disposed(by: disposableBags)
        
        viewModel.uiStateObservable.subscribe { event in
            guard let uiState = event.element else {
                return
            }
            switch uiState {
            case .loading:
                break
            default:
                break
            }
        }.disposed(by: disposableBags)
    }
}



extension AddToFleetViewController {
    @objc func keyboardWillShow(_ notification:Notification) {
        //if self.loginShown == true {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        keyboardHeight = keyboardRectangle.height - 55
        showLoginButton(true)
        //}
    }
    
    func showLoginButton(_ show: Bool) {
        if show {
            guard let keyHeight = self.keyboardHeight else {
                addToFleetConstraint.constant = -250
                UIView.animate(withDuration: 0.4, animations: {
                    self.view.layoutIfNeeded()
                })
                return
            }
            print("Height : \(keyHeight)")
            addToFleetConstraint.constant = -keyHeight - 55.0
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
            
        } else {
            addToFleetConstraint.constant = 0
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func keyboardHideShow(_ notification:Notification) {
        showLoginButton(false)
    }
    
    @objc func handleHideKeyboard(_ tapGesture: UITapGestureRecognizer) {
        guard let tf = self.currentTextField else { return }
        tf.resignFirstResponder()
        showLoginButton(false)
    }
    
    private func configureKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardHideShow(_:)),
                                               name: UIWindow.keyboardWillHideNotification, object: nil)
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.handleHideKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleAddToGroup() {
        
    }
    
}
