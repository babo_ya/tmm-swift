//
//  AddToFleetViewModel.swift
//  TrackMyMobile
//
//  Created by Seun O on 5/21/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit
import CoreData
import RxSwift
import RxCocoa

enum AddToFleetUiState {
    case loading
    case success
    case validated
    case failure(error: AddToFleetError?)
}

enum AddToFleetError: Error {
    case missingCode
    case missingFirstName
    case missingLastName
}

struct AddToFleeViewModel {
    
    let syncCoordinator: UserSyncCoordinator
    let issuedCode = BehaviorRelay<String>(value: "")
    let emailBehavior = BehaviorRelay<String>(value: "")
    let lastName = BehaviorRelay<String>(value: "")
    
    let uiStateObservable: PublishSubject<AddToFleetUiState> = PublishSubject<AddToFleetUiState>()
    let disposeBag: DisposeBag = DisposeBag()
    
    init(syncCoordinator: AppSyncCoordinator) {
        self.syncCoordinator = syncCoordinator as! UserSyncCoordinator
        self.configureUiObservables()
    }
    
    func configureUiObservables() {
        issuedCode.subscribe(onNext: { code in
            print("Code : \(code)")
        }).disposed(by: disposeBag)
        
        emailBehavior.subscribe(onNext: { code in
            print("firstName : \(code)")
        }).disposed(by: disposeBag)
        
    }
    
    private func validateUserInputs() {
        let code = issuedCode.value
        let email = self.emailBehavior.value
        
        guard !code.isEmpty else {
            uiStateObservable.onNext(AddToFleetUiState.failure(error: .missingCode))
            return
        }
        
        guard !email.isEmpty else {
            uiStateObservable.onNext(AddToFleetUiState.failure(error: .missingFirstName))
            return
        }
        
        uiStateObservable.onNext(.validated)
    }
    
    func sendAddToFleet() {
        
    }
}
