//
//  LoginViewController.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 5/13/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController {
    var titleLabel: UILabel!
    var userNameTextField: AppTextField!
    var passwordTextField: AppTextField!
    var loginButtonConstraint: NSLayoutConstraint!
    var signUpButton: UIButton!
    var registerButton: UIButton!
    var addMeToFleetButton: UIButton!
    var keyboardHeight: CGFloat = 0
    var currentTextField: UITextField?
    
    private(set) var viewModel: LoginViewModel!
    private var disposableBags: DisposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func configure(with model: LoginViewModel) {
        self.viewModel = model
        
        configureViews()
//        configureViewModel()
    }
    
    func configureViews() {
        titleLabel = UILabel()
        titleLabel = UILabel()
        titleLabel.font = AppFont.bodyBold.getFont
        titleLabel.textColor = AppColor.darkLabelColor.getColor
        titleLabel.text = "Sign In"
        self.view.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        titleLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 80).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 55).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        titleLabel.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: -30).isActive = true
        
        userNameTextField = AppTextField()
        userNameTextField.textFieldHintLabel.text = "User Name"
        userNameTextField.textField.autocapitalizationType = .none
        self.view.addSubview(userNameTextField)
        userNameTextField.layoutUIViewBelowView(belowView: self.titleLabel, padding: UIEdgeInsets(top: 45, left: 15, bottom: 15, right: 15))
        userNameTextField.textField.becomeFirstResponder()
        
        passwordTextField = AppTextField()
        passwordTextField.textFieldHintLabel.text = "Password"
        passwordTextField.textField.autocapitalizationType = .none
        passwordTextField.textField.isSecureTextEntry = true
        self.view.addSubview(passwordTextField)
        passwordTextField.layoutUIViewBelowView(belowView: self.userNameTextField)
        
        registerButton = UIButton()
        self.view.addSubview(registerButton)
        registerButton.cleanButton()
        registerButton.setTitle("Register", for: .normal)
        registerButton.translatesAutoresizingMaskIntoConstraints = false
        registerButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        registerButton.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: 30).isActive = true
        registerButton.addTarget(self, action: #selector(handleRegisterButtonTapp), for: .touchUpInside)
        
        addMeToFleetButton = UIButton()
        self.view.addSubview(addMeToFleetButton)
        addMeToFleetButton.setTitle("Add me to fleet", for: .normal)
        addMeToFleetButton.translatesAutoresizingMaskIntoConstraints = false
        addMeToFleetButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        addMeToFleetButton.topAnchor.constraint(equalTo: self.registerButton.bottomAnchor, constant: 20).isActive = true
        addMeToFleetButton.setTitleColor(AppColor.primaryColor.getColor, for: .normal)
        addMeToFleetButton.titleLabel?.font = AppFont.smallBoldFont.getFont
        
        signUpButton = UIButton()
        self.view.addSubview(signUpButton)
        signUpButton.setTitle("Sign In", for: .normal)
        signUpButton.setTitleColor(AppColor.primaryColor.getColor, for: .normal)
        signUpButton.applyDarkTheme()
        signUpButton.translatesAutoresizingMaskIntoConstraints = false
        signUpButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
        signUpButton.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        signUpButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        loginButtonConstraint = signUpButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        loginButtonConstraint?.isActive = true
        self.view.layoutIfNeeded()
        
        self.showLoginButton(true)
        configureKeyboardNotifications()
        configureViewModel()
    }
    
    @objc func handleRegisterButtonTapp() {
        
    }
    
    private func configureKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardHideShow(_:)),
                                               name: UIWindow.keyboardWillHideNotification, object: nil)
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.handleHideKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        //if self.loginShown == true {
            let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height - 55
            showLoginButton(true)
        //}
    }
    
    @objc func keyboardHideShow(_ notification:Notification) {
        showLoginButton(false)
    }
    
    @objc func handleHideKeyboard(_ tapGesture: UITapGestureRecognizer) {
        guard let tf = self.currentTextField else { return }
        tf.resignFirstResponder()
        showLoginButton(false)
    }

    func showLoginButton(_ show: Bool) {
        if show {
            print("Height : \(self.keyboardHeight)")
            loginButtonConstraint.constant = -self.keyboardHeight - 55.0
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
            
        } else {
            loginButtonConstraint.constant = 0
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    private func configureViewModel() {

        passwordTextField.textField.rx.text
            .orEmpty
            .bind(to: viewModel.passwordBehavior)
            .disposed(by: disposableBags)
        
        userNameTextField.textField.rx.text
            .orEmpty
            .bind(to: viewModel.userNameBehavior)
            .disposed(by: disposableBags)
        
        signUpButton.rx.tap
            .bind {
                self.viewModel.loginUser()
            }
            .disposed(by: disposableBags)
        
        registerButton.rx.tap
            .bind {
                self.performSegue(withIdentifier: "showRegisterViewController", sender: nil)
            }
            .disposed(by: disposableBags)
        
        addMeToFleetButton.rx.tap
            .bind {
                self.performSegue(withIdentifier: "addToFleetViewController", sender: nil)
            }
            .disposed(by: disposableBags)
        
        viewModel.uiStateObservable.subscribe { event in
            guard let uiState = event.element else {
                return
            }
            switch uiState {
            case .loading:
                self.displayLoading(true)
                self.signUpButton.disableButton()
                break
            case .none:
                //self.displayLoading(false)
                self.signUpButton.disableButton()
                break
            case .success:
                self.displayLoading(false)
                self.dismiss(animated: true, completion: nil)
                break
            case .validated:
                //self.displayLoading(false)
                self.signUpButton.applyDarkTheme()
                break
            case .invalid:
                self.signUpButton.disableButton()
                break
            case let .failure(e):
                self.displayErrorMessage(errorMessage: e)
                self.displayLoading(false)
                self.signUpButton.disableButton()
                break
            }
            }
            .disposed(by: disposableBags)
    }
    
    private func displayErrorMessage(errorMessage: String?) {
        self.passwordTextField.textField.text = ""
        let alert: UIAlertController = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func displayLoading(_ show: Bool) {
        if (show) {
            SVProgressHUD.show(withStatus: "Signing In...")
        } else {
            SVProgressHUD.dismiss()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRegisterViewController",
            let registerVC = segue.destination as? RegisterViewController {
            let viewModel: RegisterViewModel = RegisterViewModel(coordinator: self.viewModel.syncCoordinator)
            registerVC.configureWith(viewModel: viewModel)
            //registerVC.configure(with: )
            return
        }
        
        if segue.identifier == "addToFleetViewController",
            let addToFleetViewController = segue.destination as? AddToFleetViewController {
            let viewModel: AddToFleeViewModel = AddToFleeViewModel(syncCoordinator: self.viewModel.syncCoordinator)
            addToFleetViewController.configure(with: viewModel)
        }
    }
}
