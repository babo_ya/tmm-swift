//
//  RegisterViewModel.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 6/9/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

struct RegisterViewModel {
    
    let syncCoordinator: UserSyncCoordinator
    let userNameBehavior = BehaviorRelay<String>(value: "")
    let passwordBehavior = BehaviorRelay<String>(value: "")
    let emailBehavior = BehaviorRelay<String>(value: "")
    
    let uiStateObservable: PublishSubject<LoginUiState> = PublishSubject<LoginUiState>()
    let disposeBag: DisposeBag = DisposeBag()
    
    init(coordinator: UserSyncCoordinator) {
        syncCoordinator = coordinator
        configureUiObservables()
    }
    
    private func configureUiObservables() {
        userNameBehavior.subscribe(onNext: { userName in
            self.validateUserData()
        }).disposed(by: disposeBag)
        
        passwordBehavior.subscribe(onNext: { password in
            self.validateUserData()
        }).disposed(by: disposeBag)
        
        emailBehavior.subscribe(onNext: { password in
            self.validateUserData()
        }).disposed(by: disposeBag)
    }
    
    private func validateUserData() {
        let userName = userNameBehavior.value
        let password = passwordBehavior.value
        let email = emailBehavior.value
        
        guard !userName.isEmpty, userName.count > 2 else {
            uiStateObservable.onNext(LoginUiState.invalid(error: LoginViewError.missingUserName))
            return
        }
        
        guard !password.isEmpty, password.count > 2 else {
            uiStateObservable.onNext(LoginUiState.invalid(error: LoginViewError.missingPassword))
            return
        }
        
        guard !email.isEmpty, email.count > 2 else {
            uiStateObservable.onNext(LoginUiState.invalid(error: LoginViewError.missingEmail))
            return
        }
        
        uiStateObservable.onNext(LoginUiState.validated)
    }
    
    func registerUser() {
        uiStateObservable.onNext(LoginUiState.loading)
        syncCoordinator.registerUser(userName: userNameBehavior.value,
                                     password: passwordBehavior.value,
                                     email: emailBehavior.value) { (result) in
                                        switch result {
                                        case .success(_):
                                        self.uiStateObservable.onNext(LoginUiState.success)
                                            break
                                        case let .failure(e):
                                        self.uiStateObservable.onNext(LoginUiState.failure(error: e.description))
                                            break
                                        }
        }
    }
}
