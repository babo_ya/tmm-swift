//
//  RegisterViewController.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 6/7/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit
import RxSwift
import CoreData
import RxCocoa

class RegisterViewController: UIViewController {
    var titleLabel: UILabel!
    var userNameTextField: AppTextField!
    var emailTextField: AppTextField!
    var passwordTextField: AppTextField!
    var keyboardHeight: CGFloat?
    var registerConstraint: NSLayoutConstraint!
    var registerButton: UIButton!
    var addMeToFleetButton: UIButton!
    var currentTextField: UITextField?
    var userSyncCoordinator: UserSyncCoordinator!
    
    private(set) var viewModel: RegisterViewModel!
    private var disposableBags: DisposeBag = DisposeBag()
    
    func configureWith(viewModel: RegisterViewModel) {
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Register"
        configureViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        let navigationBar = navigationController!.navigationBar
        navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func configureViews() {
        userNameTextField = AppTextField()
        userNameTextField.textFieldHintLabel.text = "User Name"
        self.view.addSubview(userNameTextField)
        userNameTextField.translatesAutoresizingMaskIntoConstraints = false
        userNameTextField.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        userNameTextField.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 130).isActive = true
        userNameTextField.heightAnchor.constraint(equalToConstant: 55).isActive = true
        userNameTextField.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        userNameTextField.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: -30).isActive = true
        userNameTextField.textField.autocapitalizationType = .none
        
        passwordTextField = AppTextField()
        passwordTextField.textFieldHintLabel.text = "Password"
        self.view.addSubview(passwordTextField)
        passwordTextField.layoutUIViewBelowView(belowView: self.userNameTextField)
        passwordTextField.textField.isSecureTextEntry = true
        passwordTextField.textField.autocapitalizationType = .none
        
        emailTextField = AppTextField()
        emailTextField.textFieldHintLabel.text = "Email"
        self.view.addSubview(emailTextField)
        emailTextField.layoutUIViewBelowView(belowView: self.passwordTextField)
        emailTextField.textField.keyboardType = .emailAddress
        emailTextField.textField.autocapitalizationType = .none
        
        addMeToFleetButton = UIButton()
        self.view.addSubview(addMeToFleetButton)
        addMeToFleetButton.translatesAutoresizingMaskIntoConstraints = false
        addMeToFleetButton.setTitle("Add me to fleet", for: .normal)
        addMeToFleetButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        addMeToFleetButton.topAnchor.constraint(equalTo: self.emailTextField.bottomAnchor, constant: 30).isActive = true
        addMeToFleetButton.setTitleColor(AppColor.primaryColor.getColor, for: .normal)
        addMeToFleetButton.titleLabel?.font = AppFont.smallBoldFont.getFont
        
        registerButton = UIButton()
        self.view.addSubview(registerButton)
        registerButton.setTitle("Register", for: .normal)
        registerButton.setTitleColor(AppColor.primaryColor.getColor, for: .normal)
        registerButton.disableButton()
        registerButton.translatesAutoresizingMaskIntoConstraints = false
        registerButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
        registerButton.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        registerButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        registerConstraint = registerButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor,
                                                                    constant: 0)
        registerConstraint?.isActive = true
        self.view.layoutIfNeeded()
        
        configureKeyboardNotifications()
        configureViewModel()
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        //if self.loginShown == true {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        keyboardHeight = keyboardRectangle.height - 55
        showLoginButton(true)
        //}
    }
    
    func showLoginButton(_ show: Bool) {
        if show {
            guard let keyHeight = self.keyboardHeight else {
                registerConstraint.constant = -250
                UIView.animate(withDuration: 0.4, animations: {
                    self.view.layoutIfNeeded()
                })
                return
            }
            print("Height : \(keyHeight)")
            registerConstraint.constant = -keyHeight - 55.0
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
            
        } else {
            registerConstraint.constant = 0
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func keyboardHideShow(_ notification:Notification) {
        showLoginButton(false)
    }
    
    @objc func handleHideKeyboard(_ tapGesture: UITapGestureRecognizer) {
        guard let tf = self.currentTextField else { return }
        tf.resignFirstResponder()
        showLoginButton(false)
    }
    
    private func configureKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardHideShow(_:)),
                                               name: UIWindow.keyboardWillHideNotification, object: nil)
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.handleHideKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleAddToGroup() {
        
    }
    
    private func configureViewModel() {
        userNameTextField.textField.rx.text
            .orEmpty
            .bind(to: viewModel.userNameBehavior)
            .disposed(by: disposableBags)
        
        passwordTextField.textField.rx.text
            .orEmpty
            .bind(to: viewModel.passwordBehavior)
            .disposed(by: disposableBags)
        
        emailTextField.textField.rx.text
            .orEmpty
            .bind(to: viewModel.emailBehavior)
            .disposed(by: disposableBags)
        
        registerButton.rx.tap
            .bind {
                self.viewModel.registerUser()
            }
            .disposed(by: disposableBags)
        
        viewModel.uiStateObservable.subscribe { event in
            guard let uiState = event.element else {
                return
            }
            switch uiState {
            case .loading:
                self.displayLoading(true)
                self.registerButton.disableButton()
                break
            case .none:
                self.registerButton.disableButton()
                break
            case .success:
                self.displayLoading(false)
                self.dismiss(animated: true, completion: nil)
                break
            case .validated:
                self.registerButton.applyDarkTheme()
                break
            case .invalid:
                self.registerButton.disableButton()
                break
            case let .failure(e):
                self.displayErrorMessage(errorMessage: e)
                self.displayLoading(false)
                self.registerButton.disableButton()
                break
            }
            }
            .disposed(by: disposableBags)
    }
    
    private func displayErrorMessage(errorMessage: String?) {
        self.passwordTextField.textField.text = ""
        let alert: UIAlertController = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func displayLoading(_ show: Bool) {
        if (show) {
            SVProgressHUD.show(withStatus: "Signing In...")
        } else {
            SVProgressHUD.dismiss()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAddToFleetViewController",
            let addToFleetViewController = segue.destination as? AddToFleetViewController {
            let viewModel: AddToFleeViewModel = AddToFleeViewModel(syncCoordinator: self.viewModel.syncCoordinator)
            addToFleetViewController.configure(with: viewModel)
        }

    }
}
