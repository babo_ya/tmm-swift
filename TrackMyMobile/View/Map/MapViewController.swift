//
//  MapViewController.swift
//  TrackMyMobile
//
//  Created by Seyun oh on 6/10/19.
//  Copyright © 2019 Seyun oh. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    private(set) var viewModel: MapViewModel!
    private(set) var locationViewModel: LocationViewModel!
    let locationManager = CLLocationManager()
    
    var locsationAuthorized: Bool {
        return CLLocationManager.authorizationStatus() == .authorizedAlways ||
        CLLocationManager.authorizationStatus() == .authorizedWhenInUse
    }
    
    func configure(with viewModel: MapViewModel, locationViewModel: LocationViewModel) {
        self.locationViewModel = locationViewModel
        self.viewModel = viewModel
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self.locationViewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
}

